*** Settings ***
Documentation    Test integration jenkins and testrail
Library    Selenium2Library
Library    String
Library  OperatingSystem

Suite Setup			SUITE:Setup
Suite Teardown		SUITE:Teardown

*** Variables ***
${BROWSER}				headlessfirefox
# ${BROWSER}				firefox
${HOME_URL}				https://192.168.2.90

${USERNAME}            admin
${PASSWORD}            @locomia

${TITLE_PAGE_LOGIN}			Nodegrid
${FIELD_USERNAME}        //input[@id='username']
${FIELD_PASSWORD}        //input[@id='password']
${BUTTON_LOGIN}            //input[@id='login-btn']
${BUTTON_CONSOLE}            //a[contains(text(),'Console')]
${TITLE_PAGE_ACCESS}    VM-2-90 - Nodegrid
${PATH_NAME}			ftp://192.168.2.201/nodegrid-genericx86-64-20230417003548_Master-20230416173002.signed.iso
*** Test Case ***
Test to Access Nodegrid Homepage
    [Documentation]    Test to access nodegrid homepage
	Go To								${HOME_URL}
	Wait Until Element Is Visible		${FIELD_USERNAME}    timeout=10
	Title Should Be 					${TITLE_PAGE_LOGIN}
	Capture Page Screenshot

Test to Login on nodegrid
    [Documentation]    Test to login nodegrid
	Input Text		             ${FIELD_USERNAME}	${USERNAME}
	Input Text		             ${FIELD_PASSWORD}	${PASSWORD}
	Click Element                ${BUTTON_LOGIN}
	Wait Until Page Contains Element        ${BUTTON_CONSOLE}    timeout=10
	Page Should Contain Element			    ${BUTTON_CONSOLE}
	Title Should Be 			    ${TITLE_PAGE_ACCESS}
	Capture Page Screenshot

*** Keywords ***
SUITE:Setup
	Open Browser	about:blank		${BROWSER}
	Write the iso name in a txt			${PATH_NAME}	3.2

SUITE:Teardown
	close all browsers

Write the iso name in a txt
	[Documentation]		Write the iso name inside a txt file in the informations folder
	[Arguments]  ${ISO_NAME}	${NODEGRID_VERSION}
	Create File  informations/nodegrid-iso-name-${NODEGRID_VERSION}.txt  ${ISO_NAME}











