# EX: python create_milestone.py 3.2
import requests
from datetime import date
import sys as arguments


get_url_testrail_milestones = "https://zpesystems.testrail.io/index.php?/api/v2/get_milestones/7"
post_url_testrail_milestones = "https://zpesystems.testrail.io/index.php?/api/v2/add_milestone/7"
username = "filipe.vansuita@zpesystems.com"
key = "x5d5Dap2ExQv.L07eiBQ-xmHKZDbGRcz47tVZ5Jqw"

# arg_milestone_name = arguments.argv[1]
arg_nodegrid_version = arguments.argv[1]

today = date.today()
date = today.strftime("%d/%m/%Y")
milestone_name = "Nodegrid "+arg_nodegrid_version+" - "+date

### CREATE A NEW MILESTONE ###
data_new_milestone = {'name': milestone_name}
add_new_milestone = requests.post(post_url_testrail_milestones, auth=(username, key), json = data_new_milestone)

### GET LAST MILESTONE ###
milestones = requests.get(url=get_url_testrail_milestones, auth=(username, key))
all_milestones = milestones.json()
size = len(all_milestones["milestones"])
last_milestone = all_milestones["milestones"][size-1]
last_milestone_id = last_milestone["id"]

### WRITE MILESTONE ID ON TXT ###
file = open("informations/milestone-nodegrid-"+arg_nodegrid_version+".txt", "w")
file.write(str(last_milestone_id))
file.close()