### EX: python create_reports.py testrun-name 3.2
import sys as arguments

### VARIABLES ###
get_url_testrail_milestones = "https://zpesystems.testrail.io/index.php?/api/v2/get_milestones/7"
post_url_testrail_milestones = "https://zpesystems.testrail.io/index.php?/api/v2/add_milestone/7"
username = "filipe.vansuita@zpesystems.com"
key = "x5d5Dap2ExQv.L07eiBQ-xmHKZDbGRcz47tVZ5Jqw"
versions_ids = {'3.2': '1', '5.0': '2', '5.2': '3', '5.4': '4', '5.6': '5', '5.8': '6', '5.10': '7'}

### ARGUMENTS FROM SHELL ###
arg_testrun_name = arguments.argv[1]
arg_nodegrid_version = arguments.argv[2]

TESTRUN_TITLE = arg_testrun_name
print("Title of Testrun: "+TESTRUN_TITLE)

VERSION_ID = versions_ids[arg_nodegrid_version]

### GET ISO NAME FROM FILE ###
file = open("informations/nodegrid-iso-name-"+arg_nodegrid_version+".txt", "r")
ISO_NAME = file.read()
print("Name of ISO: "+ISO_NAME)

### GET MILESTONE ID FROM FILE ###
file = open("informations/milestone-nodegrid-"+arg_nodegrid_version+".txt", "r")
LAST_MILESTONE_ID = file.read()
print("Milestone ID: "+LAST_MILESTONE_ID)

### CREATE YML FILE ###
yml_host = "host: https://zpesystems.testrail.io\n"
yml_username = "username: "+username+"\n"
yml_key = "key: "+key+"\n"
yml_project = "project: QA Test\n"
yml_title = "title: "+TESTRUN_TITLE+"\n"
yml_file = "file: results/reports.xml\n"
yml_result_fields = "result_fields:\n   custom_test_version: "+VERSION_ID+"\n"
yml_case_fields = "case_fields:\n   custom_application_name: 1\n   custom_application_versions: 1\n"
yml_description = "run_description: "+ISO_NAME+"\n"
yml_milestone_id = "milestone_id: "+str(LAST_MILESTONE_ID)+"\n"

file = open("trcli-config/reports.yml", "w")
file.write(yml_host)
file.write(yml_username)
file.write(yml_key)
file.write(yml_project)
file.write(yml_title)
file.write(yml_file)
file.write(yml_result_fields)
file.write(yml_case_fields)
file.write(yml_description)
file.write(yml_milestone_id)
file.close()